CREATE TABLE user (
  id          INT PRIMARY KEY AUTO_INCREMENT,
  email       VARCHAR(255) NOT NULL,
  password    VARCHAR(255) NOT NULL,
  enabled     BOOLEAN         DEFAULT FALSE,
  registered  DATE,
  role        VARCHAR(15)
);
INSERT INTO user (email, password, enabled, registered, role)
VALUES ('user01@mail.ru', '1', TRUE , '2000-01-01', 'ROLE_USER');
INSERT INTO user (email, password, enabled, registered, role)
VALUES ('admin01@mail.ru', '1', TRUE , '2000-01-01', 'ROLE_USER');

CREATE TABLE type_car (
  id          INT PRIMARY KEY AUTO_INCREMENT,
  name       VARCHAR(255) NOT NULL,
);
INSERT INTO type_car (name) VALUES ('грузовая 20т');
INSERT INTO type_car (name) VALUES ('грузовая 10т');
INSERT INTO type_car (name) VALUES ('грузовая 5т');
INSERT INTO type_car (name) VALUES ('автобус 40м');
INSERT INTO type_car (name) VALUES ('автобус 20м');
INSERT INTO type_car (name) VALUES ('микроавтобус 10м');
INSERT INTO type_car (name) VALUES ('минивен 7м');
INSERT INTO type_car (name) VALUES ('легковая');

create table employee(
  id          INT PRIMARY KEY AUTO_INCREMENT,
  type        VARCHAR(15) NOT NULL,
  email       VARCHAR(255) NOT NULL,
  full_name    VARCHAR(255) NOT NULL
);
INSERT INTO employee (type, email,full_name) VALUES ('DRIVER', 'driver01@mail.ru', 'Иванов Иван Иванович');
INSERT INTO employee (type, email,full_name) VALUES ('DRIVER', 'driver02@mail.ru', 'Петров Петр Петрович');
INSERT INTO employee (type, email,full_name) VALUES ('DRIVER', 'driver03@mail.ru', 'Сидоров Николай Николаевич');
INSERT INTO employee (type, email,full_name) VALUES ('DISPATCHER', 'driver01@mail.ru', 'Хвостов Сергей Сергеевич');
INSERT INTO employee (type, email,full_name) VALUES ('DISPATCHER', 'driver01@mail.ru', 'Миронов Алексей Алексеевич');

CREATE TABLE car(
  id          INT PRIMARY KEY AUTO_INCREMENT,
  reg_number   VARCHAR(15) NOT NULL UNIQUE ,
  model       VARCHAR(15),
  color       VARCHAR(15),
  driver_id   INT NOT NULL,
  type_car_id INT NOT NULL,
  FOREIGN KEY (driver_id) REFERENCES employee(id),
  FOREIGN KEY (type_car_id) REFERENCES type_car(id)
);
insert into car (reg_number, model, color, driver_id, type_car_id) VALUES ('а001аа','volvo','красный',1,1);
insert into car (reg_number, model, color, driver_id, type_car_id) VALUES ('а002аа','scania','зеленый',2,2);
insert into car (reg_number, model, color, driver_id, type_car_id) VALUES ('а003аа','ford','черный',3,3);

create table stakeholder(
  id          INT PRIMARY KEY AUTO_INCREMENT,
  fullName    VARCHAR(255) NOT NULL,
  description VARCHAR(255)
);
INSERT INTO stakeholder (fullName, description) VALUES ('Владивостоков Владислав', 'Владивосток');
INSERT INTO stakeholder (fullName, description) VALUES ('Рязанцев Вениамин', 'Рязань');
INSERT INTO stakeholder (fullName, description) VALUES ('Ростовцев Роман', 'Ростов');

create table run(
  id          INT PRIMARY KEY AUTO_INCREMENT,
  name        VARCHAR(255) NOT NULL,
  car_id      INT,
  dispatcher_id INT,
  client_id INT,
  state       VARCHAR(15),
  FOREIGN KEY (car_id) REFERENCES car(id),
  FOREIGN KEY (dispatcher_id) REFERENCES employee(id),
  FOREIGN KEY (client_id) REFERENCES stakeholder(id)
);
INSERT INTO run (name, car_id, dispatcher_id, client_id, state) VALUES ('Москва-Владивосток', 1, 4, 1, 'ENDED');
INSERT INTO run (name, car_id, dispatcher_id, client_id, state) VALUES ('Москва-Рязань', 2, 5, 2, 'ENDED');
INSERT INTO run (name, car_id, dispatcher_id, client_id, state) VALUES ('Москва-Ростов', 3, 4, 1, 'ENDED');
INSERT INTO run (name, car_id, dispatcher_id, client_id, state) VALUES ('Москва-Владивосток', 1, 4, 1, 'CANCELED');
INSERT INTO run (name, car_id, dispatcher_id, client_id, state) VALUES ('Москва-Рязань', 2, 5, 2, 'CANCELED');
INSERT INTO run (name, car_id, dispatcher_id, client_id, state) VALUES ('Москва-Ростов', 3, 4, 1, 'CANCELED');
INSERT INTO run (name, car_id, dispatcher_id, client_id, state) VALUES ('Москва-Владивосток', 1, 4, 1, 'RUNNING');
INSERT INTO run (name, car_id, dispatcher_id, client_id, state) VALUES ('Москва-Рязань', 2, 5, 2, 'RUNNING');
INSERT INTO run (name, car_id, dispatcher_id, client_id, state) VALUES ('Москва-Ростов', 3, 4, 1, 'READY');
