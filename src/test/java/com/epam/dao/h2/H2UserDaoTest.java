package com.epam.dao.h2;

import com.epam.dao.DaoFactory;
import com.epam.dao.H2DaoFactory;
import com.epam.dao.interfaces.UserDao;
import com.epam.jdbc.ConnectionPool;
import com.epam.model.Role;
import com.epam.model.User;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import static org.junit.Assert.*;


public class H2UserDaoTest {

    private static final String RESOURCES_FILE_PATH = "src/test/resources/";
    private static final String DB_PROPERTIES_FILE_NAME = "db.properties";
    private static final String DB_PREPARE_FILE_NAME = "h2.sql";
    private static DaoFactory daoFactory;


    @BeforeClass
    public static void prepare() throws Exception {
        ConnectionPool connectionPool =
                ConnectionPool.create(RESOURCES_FILE_PATH + DB_PROPERTIES_FILE_NAME);
        connectionPool.executeScript(RESOURCES_FILE_PATH + DB_PREPARE_FILE_NAME);

        daoFactory = new H2DaoFactory( connectionPool );
    }

    @Test
    public void getById() throws Exception {
        final User user = daoFactory.getUserDao().getById(1)
                .orElseThrow(() -> new RuntimeException("There is no such User with id = " + 1));

        assertThat(user.getEmail(), is("user01@mail.ru"));
    }

    @Test
    public void getByEmail() throws Exception {
        final User user = daoFactory.getUserDao().getByEmail("user01@mail.ru")
                .orElseThrow(() -> new RuntimeException("There is no such User with email user01@mail.ru"));
    }

    @Test
    public void getByRole() throws Exception {
        final List<User> users = daoFactory.getUserDao().getByRole(Role.ROLE_USER);

        if (users.isEmpty()) {
            new RuntimeException("There is no such User with email user01@mail.ru");
        }

        assertThat(users.get(0).getEmail(), is("user01@mail.ru"));
        assertEquals( 2, users.size());
    }

    @Test
    public void getAll() throws Exception {

    }

}