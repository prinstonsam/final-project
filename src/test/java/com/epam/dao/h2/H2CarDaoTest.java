package com.epam.dao.h2;

import com.epam.dao.DaoFactory;
import com.epam.dao.H2DaoFactory;
import com.epam.dao.interfaces.CarDao;
import com.epam.jdbc.ConnectionPool;
import com.epam.model.Car;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class H2CarDaoTest {

	private static final String RESOURCES_FILE_PATH = "src/test/resources/";
	private static final String DB_PROPERTIES_FILE_NAME = "db.properties";
	private static final String DB_PREPARE_FILE_NAME = "h2.sql";
	private static DaoFactory daoFactory;


	@BeforeClass
	public static void prepare() throws Exception {
		ConnectionPool connectionPool =
				ConnectionPool.create(RESOURCES_FILE_PATH + DB_PROPERTIES_FILE_NAME);
		connectionPool.executeScript(RESOURCES_FILE_PATH + DB_PREPARE_FILE_NAME);

		daoFactory = new H2DaoFactory( connectionPool );
	}

	@Test
	public void getById( ) throws Exception {
		final Car car = daoFactory.getCarDao().getById(1)
				.orElseThrow(() -> new RuntimeException("There is no such Car with id = " + 1));

		assertThat(car.getRegNumber(), is("а001аа"));
	}

}