package com.epam.dao.h2;

import com.epam.dao.DaoFactory;
import com.epam.dao.H2DaoFactory;
import com.epam.dao.interfaces.RunDao;
import com.epam.jdbc.ConnectionPool;
import com.epam.model.Run;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class H2RunDaoTest {
	private static final String RESOURCES_FILE_PATH = "src/test/resources/";
	private static final String DB_PROPERTIES_FILE_NAME = "db.properties";
	private static final String DB_PREPARE_FILE_NAME = "h2.sql";
	private static DaoFactory daoFactory;




	@BeforeClass
	public static void prepare() throws Exception {
		ConnectionPool connectionPool =
				ConnectionPool.create(RESOURCES_FILE_PATH + DB_PROPERTIES_FILE_NAME);
		connectionPool.executeScript(RESOURCES_FILE_PATH + DB_PREPARE_FILE_NAME);

		daoFactory = new H2DaoFactory( connectionPool );
	}
	@Test
	public void getByName( ) throws Exception {
		final List<Run> runs = daoFactory.getRunDao().getByName("Владивосток");
		if ( runs.isEmpty( ) ) {
			new RuntimeException("There is no such Runs with name  like " + "Владивосток");
		}

		assertEquals(3, runs.size());
	}

	@Test
	public void getAll( ) throws Exception {
		final List<Run> runs = daoFactory.getRunDao().getAll();

		assertEquals(9, runs.size());
	}
}