package com.epam.dao.h2;

import com.epam.dao.DaoFactory;
import com.epam.dao.H2DaoFactory;
import com.epam.dao.interfaces.CarDao;
import com.epam.dao.interfaces.EmployeeDao;
import com.epam.jdbc.ConnectionPool;
import com.epam.model.Car;
import com.epam.model.Employee;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class H2EmployeeDaoTest {
	private static final String RESOURCES_FILE_PATH = "src/test/resources/";
	private static final String DB_PROPERTIES_FILE_NAME = "db.properties";
	private static final String DB_PREPARE_FILE_NAME = "h2.sql";
	private static DaoFactory daoFactory;


	@BeforeClass
	public static void prepare() throws Exception {
		ConnectionPool connectionPool =
				ConnectionPool.create(RESOURCES_FILE_PATH + DB_PROPERTIES_FILE_NAME);
		connectionPool.executeScript(RESOURCES_FILE_PATH + DB_PREPARE_FILE_NAME);

		daoFactory = new H2DaoFactory( connectionPool );
	}
	@Test
	public void getById( ) throws Exception {
		final Employee employee = daoFactory.getEmployeeDao().getById(1)
				.orElseThrow(() -> new RuntimeException("There is no such User with id = " + 1));

		assertThat(employee.getFullName(), is("Иванов Иван Иванович"));
	}

}