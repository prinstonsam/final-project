package com.epam.tag;

import com.epam.model.Run;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Formatter;

public class RunTag extends TagSupport {
	private Run value;

	public void setValue(Run value) {
		this.value = value;
	}

	public int doStartTag() throws JspException {

/*
		LocaleConfig langConfig=LocaleConfig.getInstance();
		String brand=langConfig.getText(LocaleConfig.CAR_BRAND);
		String model=langConfig.getText(LocaleConfig.CAR_MODEL);
		String classCar=langConfig.getText(LocaleConfig.CAR_CLASS);
		String quantityDoors=langConfig.getText(LocaleConfig.CAR_QUANTITY_DOORS);
		String quantitySeats=langConfig.getText(LocaleConfig.CAR_QUANTITY_SEATS);
		String transmission=langConfig.getText(LocaleConfig.CAR_TRANSMITION);
		String price=langConfig.getText(LocaleConfig.CAR_PRICE);
		String order=langConfig.getText(LocaleConfig.ORDER);
*/

		StringBuilder stringFormat = new StringBuilder();
		Formatter formatter = new Formatter(stringFormat);
		formatter.format("<tr><td>%s</td><td>%s</td></tr>","Название рейса", value.getName());
		formatter.format("<tr><td>%s</td><td>%s</td></tr>","Состояние",value.getState());

		try {
			pageContext.getOut().write(stringFormat.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return SKIP_BODY;
	}

}
