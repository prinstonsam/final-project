package com.epam.controller;

import com.epam.LoggerWrapper;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/runlist")
public class RunsServlet extends HttpServlet{
//	private LoggerWrapper logger = LoggerWrapper.get(getClass());

	protected void doGet(HttpServletRequest request,
						 HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
						  HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request,
								HttpServletResponse response) throws ServletException, IOException {

		String page = null;
		try {
			Command command = new RunListCommand();
			page = command.execute(request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		}
		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher(page);
		dispatcher.forward(request, response);
	}
}
