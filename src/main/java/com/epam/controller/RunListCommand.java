package com.epam.controller;

import com.epam.LoggerWrapper;
import com.epam.dao.DaoFactory;
import com.epam.dao.H2DaoFactory;
import com.epam.dao.interfaces.RunDao;
import com.epam.listeners.DaoProvider;
import com.epam.model.Run;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class RunListCommand implements Command {
	private LoggerWrapper logger = LoggerWrapper.get(getClass());
	@Override
	public String execute( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

		DaoFactory factory = new H2DaoFactory( DaoProvider.getConnectionPool());
		RunDao runDao = factory.getRunDao();
		List<Run> runs = runDao.getAll();
		request.setAttribute("runs", runs);

		logger.info("Redirection to the order list page for Administrator");
		return "/jsp/runlist.jsp";
	}
}

