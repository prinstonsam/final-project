package com.epam.model;

import lombok.Data;

@Data
public class TypeCar {
    public static String TABLE_NAME = "type_car";

    private Integer id;
    private String name;
}
