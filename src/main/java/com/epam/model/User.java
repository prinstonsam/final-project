package com.epam.model;

import com.epam.LoggerWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.Set;

@Data
@AllArgsConstructor
public class User {

    private Integer id;
    private String email;
    private String password;
    private boolean enabled = true;
    private Date registered = new Date();
    private Role role;
}
