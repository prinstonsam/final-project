package com.epam.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Employee {
    public static String TABLE_NAME = "employee";

    private Integer id;
    private TypeEmployee type;
    private String fullName;

}
