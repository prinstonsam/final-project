package com.epam.model;

public enum TypeEmployee {
    DRIVER,
    DISPATCHER;

    public String getType() {
        return name();
    }

    public boolean isDriver(TypeEmployee typeEmployee) {
        if (typeEmployee == DRIVER) {
            return true;
        }
        return false;
    }

    public boolean isDipatecher(TypeEmployee typeEmployee) {
        if (typeEmployee == DISPATCHER) {
            return true;
        }
        return false;
    }
}
