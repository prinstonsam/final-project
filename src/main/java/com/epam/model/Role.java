package com.epam.model;

public enum Role {
    ROLE_USER,
    ROLE_ADMIN;

    public String getRole() {
        return name();
    }
}
