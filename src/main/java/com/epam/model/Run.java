package com.epam.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Run {
    public static String TABLE_NAME = "run";

    private Integer id;
    private String name;
    private Integer carid;
    private Integer dispatcherId;
    private Integer clientId;
    private StateRun state;
}
