package com.epam.model;

public enum StateRun {
    READY,RUNNING,ENDED,CANCELED;

    public String getState() {
        return name();
    }

    public boolean isReady(StateRun state) {
        if (state == READY) {
            return true;
        }
        return false;
    }

    public boolean isRunning(StateRun state) {
        if (state == RUNNING) {
            return true;
        }
        return false;
    }

    public boolean isEnded(StateRun state) {
        if (state == ENDED) {
            return true;
        }
        return false;
    }

    public boolean isCanceled(StateRun state) {
        if (state == CANCELED) {
            return true;
        }
        return false;
    }
}
