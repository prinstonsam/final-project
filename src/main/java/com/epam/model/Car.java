package com.epam.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Car {
    public static String TABLE_NAME = "car";

    private Integer id;
    private String regNumber;
    private String model;
    private String color;
    private Integer driverId;
    private Integer typeCarId;
}
