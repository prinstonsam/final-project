package com.epam.model;

import lombok.Data;

@Data
public class Stakeholder {
    private Integer id;
    private String fullName;
    private String description;
}
