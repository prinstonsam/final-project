package com.epam.listeners;


import com.epam.jdbc.ConnectionPool;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class DaoProvider implements ServletContextListener {
	private static final String RESOURCES_FILE_PATH = "/WEB-INF/classes/";
	private static final String DB_PROPERTIES_FILE_NAME = "db.properties";
	private static final String DB_PREPARE_FILE_NAME = "h2.sql";

private static ConnectionPool connectionPool;

	public static ConnectionPool getConnectionPool( ) {
		return connectionPool;
	}

	@Override
	public void contextInitialized( ServletContextEvent sce ) {
		final ServletContext servletContext = sce.getServletContext();

		final String dbPropertiesFilePath = servletContext.getRealPath(RESOURCES_FILE_PATH + DB_PROPERTIES_FILE_NAME);
		final String dbPrepareFilePath = servletContext.getRealPath(RESOURCES_FILE_PATH + DB_PREPARE_FILE_NAME);

		connectionPool = ConnectionPool.create(dbPropertiesFilePath);
		connectionPool.executeScript(dbPrepareFilePath);
	}

	@Override
	public void contextDestroyed( ServletContextEvent servletContextEvent ) {
		try {
			connectionPool.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}

