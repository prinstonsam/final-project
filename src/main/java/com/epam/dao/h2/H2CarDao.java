package com.epam.dao.h2;

import com.epam.LoggerWrapper;
import com.epam.dao.interfaces.CarDao;
import com.epam.dao.interfaces.EmployeeDao;
import com.epam.dao.interfaces.TypeCarDao;
import com.epam.jdbc.ConnectionPool;
import com.epam.model.Car;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;

public class H2CarDao implements CarDao {
	private LoggerWrapper logger = LoggerWrapper.get(getClass());

	private ConnectionPool connectionPool;

	public H2CarDao(ConnectionPool connectionPool) {
		this.connectionPool = connectionPool;
	}

	Optional<Car> getResult(ResultSet rs) throws SQLException {
		return rs.next()
				? Optional.of(
				new Car(
						rs.getInt( "id" ),
						rs.getString("reg_number"),
						rs.getString("model"),
						rs.getString( "color" ),
						rs.getInt( rs.getInt( "driver_id" )),
						rs.getInt("type_car_id")))
				: Optional.empty();
	}

	@Override
	public Optional<Car> getById( int id ) {
		try ( final Connection connection = connectionPool.getConnection();
			  final Statement statement = connection.createStatement();
			  final ResultSet rs = statement.executeQuery(
					 "SELECT id, reg_number, model, color, driver_id, type_car_id FROM car WHERE id = " + id)) {
			connectionPool.freeConnection( connection );
			return getResult( rs );
		} catch (SQLException e) {
			logger.warn("Error getByid for H2CarDao");
			throw new RuntimeException(e);
		}
	}
}
