package com.epam.dao.h2;

import com.epam.LoggerWrapper;
import com.epam.dao.interfaces.EmployeeDao;
import com.epam.dao.interfaces.TypeCarDao;
import com.epam.jdbc.ConnectionPool;
import com.epam.model.Employee;
import com.epam.model.TypeEmployee;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Optional;

public class H2EmployeeDao implements EmployeeDao {
	private LoggerWrapper logger = LoggerWrapper.get( getClass( ) );

	private ConnectionPool connectionPool;

	private static EmployeeDao driverDao;

	private static TypeCarDao typeCarDao;

	public H2EmployeeDao( ConnectionPool connectionPool ) {
		this.connectionPool = connectionPool;
	}

	@Override
	public Optional<Employee> getById( int id ) {
		try ( final Connection connection = connectionPool.getConnection( );
			  final Statement statement = connection.createStatement( );
			  final ResultSet rs = statement.executeQuery( "SELECT type, full_name FROM employee WHERE id = " + id ) ) {
			connectionPool.freeConnection( connection );
			return rs.next( ) ? Optional.of(
					new Employee(
							id,
							TypeEmployee.valueOf(  rs.getString( "type" )),
							rs.getString( "full_name" )))
					: Optional.empty( );

		} catch ( SQLException e ) {
			logger.warn( "Error getByid for H2CarDao" );
			throw new RuntimeException( e );
		}
	}

	@Override
	public Optional<Employee> getByFullName( String fullName ) {
		return null;
	}

	@Override
	public List<Employee> searchByFullNameTemplate( String fullNameTemplate ) {
		return null;
	}

	@Override
	public List<Employee> getByType( TypeEmployee type ) {
		return null;
	}

	@Override
	public List<Employee> getAll( ) {
		return null;
	}
}
