package com.epam.dao.h2;

import com.epam.LoggerWrapper;
import com.epam.dao.interfaces.UserDao;
import com.epam.jdbc.ConnectionPool;
import com.epam.model.Role;
import com.epam.model.User;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class H2UserDao implements UserDao{
    private LoggerWrapper logger = LoggerWrapper.get(getClass());

    private ConnectionPool connectionPool;

    public H2UserDao(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }

    public Optional getResult(ResultSet rs) throws SQLException {
		return rs.next()
				? Optional.of(
				new User(
						rs.getInt("id"),
						rs.getString("email"),
						rs.getString("password"),
						rs.getBoolean("enabled"),
						rs.getDate("registered"),
						Role.valueOf(rs.getString("role"))))
				: Optional.empty();
	}

	public List<User> getListResult(ResultSet rs) throws SQLException {
		List<User> list = new LinkedList<>();
		while (rs.next()) {
			list.add(new User(
					rs.getInt("id"),
					rs.getString("email"),
					rs.getString("password"),
					rs.getBoolean("enabled"),
					rs.getDate("registered"),
					Role.valueOf(rs.getString("role"))));
		}
		return list;
	}

    @Override
    public Optional<User> getById(int id) {
        try (final Connection connection = connectionPool.getConnection();
             final Statement statement = connection.createStatement();
             final ResultSet rs = statement.executeQuery(
                     "SELECT id, email, password, enabled, registered, role FROM user WHERE id = " + id)) {
			connectionPool.freeConnection( connection );
			return getResult(rs);
        } catch (SQLException e) {
            logger.warn("Error getByid for H2UserDao");
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<User> getByEmail(String email) {
        try (final Connection connection = connectionPool.getConnection();
             final Statement statement = connection.createStatement();
             final ResultSet rs = statement.executeQuery(
                     "SELECT id, email, password, enabled, registered, role FROM user WHERE email = '" + email + "'")) {
			connectionPool.freeConnection( connection );
			return getResult(rs);
        } catch (SQLException e) {
            logger.warn("Error getByEmail for H2UserDao");
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<User> getByRole(Role role) {
        List<User> list = new LinkedList<>();
        try (final Connection connection = connectionPool.getConnection();
             final Statement statement = connection.createStatement();
             final ResultSet rs = statement.executeQuery(
                     "SELECT id, email, password, enabled, registered, role FROM user WHERE role = '" + role.getRole() + "'")){
			connectionPool.freeConnection( connection );
            return getListResult( rs );
        } catch (SQLException e) {
            logger.warn("Error getByRole for H2UserDao");
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<User> getAll() {
		List<User> list = new LinkedList<>();
		try (final Connection connection = connectionPool.getConnection();
			 final Statement statement = connection.createStatement();
			 final ResultSet rs = statement.executeQuery(
					 "SELECT id, email, password, enabled, registered, role FROM user")){
			connectionPool.freeConnection( connection );
			return getListResult( rs );
		} catch (SQLException e) {
			logger.warn("Error getAll for H2UserDao");
			throw new RuntimeException(e);
		}


    }
}
