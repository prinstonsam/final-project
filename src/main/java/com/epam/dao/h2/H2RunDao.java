package com.epam.dao.h2;

import com.epam.LoggerWrapper;
import com.epam.dao.interfaces.RunDao;
import com.epam.jdbc.ConnectionPool;
import com.epam.model.Employee;
import com.epam.model.Run;
import com.epam.model.StateRun;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class H2RunDao implements RunDao {
	private LoggerWrapper logger = LoggerWrapper.get(getClass());

	private ConnectionPool connectionPool;

	public H2RunDao( ConnectionPool connectionPool ) {
		this.connectionPool = connectionPool;
	}

	@Override
	public Integer save( Run run ) {
		return null;
	}

	@Override
	public void update( Run run ) {

	}

	@Override
	public void delete( Run run ) {

	}

	@Override
	public List<Run> getByName( String name ) {
		List<Run> list = new LinkedList<>();
		try ( final Connection connection = connectionPool.getConnection();
			  final Statement statement = connection.createStatement();
			  final ResultSet rs = statement.executeQuery(
					 "SELECT id, name, car_id, dispatcher_id, client_id, state FROM run WHERE name like '%" + name + "%'")){

			while (rs.next()) {
				list.add(new Run(
						rs.getInt("id"),
						rs.getString("name"),
						null,
						null,
						null,
						StateRun.valueOf( rs.getString("state"))));
			}
			connectionPool.freeConnection( connection );
			return list;
		} catch (SQLException e) {
			logger.warn("Error getByName for H2RunDao");
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Run> getRunning( ) {
		return null;
	}

	@Override
	public List<Run> getReady( ) {
		return null;
	}

	@Override
	public List<Run> getByDriver( Employee driver ) {
		return null;
	}

	@Override
	public List<Run> getByDispatcher( Employee dispatcher ) {
		return null;
	}

	@Override
	public List<Run> getAll( ) {
		List<Run> list = new LinkedList<>();
		try ( final Connection connection = connectionPool.getConnection();
			  final Statement statement = connection.createStatement();
			  final ResultSet rs = statement.executeQuery(
					  "SELECT id, name, car_id, dispatcher_id, client_id, state FROM run")){

			while (rs.next()) {
				list.add(new Run(
						rs.getInt("id"),
						rs.getString("name"),
						null,
						null,
						null,
						StateRun.valueOf( rs.getString("state"))));
			}
			connectionPool.freeConnection( connection );
			return list;
		} catch (SQLException e) {
			logger.warn("Error getAll for H2RunDao");
			throw new RuntimeException(e);
		}
	}
}
