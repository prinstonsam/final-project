package com.epam.dao;

import com.epam.LoggerWrapper;
import com.epam.dao.h2.H2CarDao;
import com.epam.dao.h2.H2EmployeeDao;
import com.epam.dao.h2.H2RunDao;
import com.epam.dao.h2.H2UserDao;
import com.epam.dao.interfaces.*;
import com.epam.jdbc.ConnectionPool;

public class H2DaoFactory implements DaoFactory {
	private LoggerWrapper logger = LoggerWrapper.get(getClass());

	ConnectionPool connectionPool;

	CarDao carDao;
	EmployeeDao employeeDao;
	RunDao runDao;
	UserDao userDao;

	public H2DaoFactory( ConnectionPool connectionPool ) {
		this.connectionPool = connectionPool;
	}

	@Override
	public CarDao getCarDao( ) {
		return carDao == null ? new H2CarDao( connectionPool ) : carDao;
	}

	@Override
	public EmployeeDao getEmployeeDao( ) {
		return employeeDao == null ? new H2EmployeeDao( connectionPool ) : employeeDao;
	}

	@Override
	public RunDao getRunDao( ) {
		return runDao == null ? new H2RunDao( connectionPool ) : runDao;
	}

	//TODO
	@Override
	public StakeHolderDao getStakeHolderDao( ) {
		return null;
	}

	@Override
	public UserDao getUserDao( ) {
		return userDao == null ? new H2UserDao( connectionPool ) : userDao;
	}

	//TODO
	@Override
	public TypeCarDao getTypeCarDao( ) {
		return null;
	}

}


