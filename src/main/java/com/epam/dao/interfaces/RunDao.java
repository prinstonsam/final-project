package com.epam.dao.interfaces;

import com.epam.model.Employee;
import com.epam.model.Run;

import java.sql.Driver;
import java.util.List;

public interface RunDao {
	public Integer save( Run run );

	public void update( Run run );

	public void delete( Run run );

	public List<Run> getByName( String name );

	public List<Run> getRunning( );

	public List<Run> getReady( );

	public List<Run> getByDriver( Employee driver );

	public List<Run> getByDispatcher( Employee dispatcher);

	public List<Run> getAll();

}
