package com.epam.dao.interfaces;


import com.epam.model.Role;
import com.epam.model.User;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface UserDao {

    Optional<User> getById(int id);

    Optional<User> getByEmail(String email);

    List<User> getByRole(Role role);

    List<User> getAll();
}
