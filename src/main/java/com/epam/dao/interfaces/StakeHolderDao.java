package com.epam.dao.interfaces;

import com.epam.model.Stakeholder;

import java.util.List;
import java.util.Optional;

public interface StakeHolderDao {

    Optional<Stakeholder> getById(int id);

    Optional<Stakeholder> getByFullName(String fullName);

    List<Stakeholder> searchByFullNameTemplate(String fullNameTemplate);

    List<Stakeholder> getAll();
}
