package com.epam.dao.interfaces;

import com.epam.model.Employee;
import com.epam.model.TypeEmployee;

import java.util.List;
import java.util.Optional;

public interface EmployeeDao {

    Optional<Employee> getById(int id);

    Optional<Employee> getByFullName(String fullName);

    List<Employee> searchByFullNameTemplate(String fullNameTemplate);

    List<Employee> getByType(TypeEmployee type);

    List<Employee> getAll();    
    
}
