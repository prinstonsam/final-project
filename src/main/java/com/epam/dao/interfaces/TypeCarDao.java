package com.epam.dao.interfaces;

import com.epam.model.TypeCar;

import java.util.Optional;

public interface TypeCarDao {
	Optional<TypeCar> getById( int id);
}
