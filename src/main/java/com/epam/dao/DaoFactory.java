package com.epam.dao;

import com.epam.dao.interfaces.*;

public interface DaoFactory {
	CarDao getCarDao();
	EmployeeDao getEmployeeDao();
	RunDao getRunDao();
	StakeHolderDao getStakeHolderDao();
	TypeCarDao getTypeCarDao();
	UserDao getUserDao();
}
