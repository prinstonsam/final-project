<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="runtag" uri="/WEB-INF/runtag.tld" %>
<%--<%@ taglib prefix="runtag" uri="/WEB-INF/runtag.tld" %>--%>
<html>
<head>
    <title>Список рейсов</title>
</head>
<body>
<div id="page">

    <div>
        <c:forEach var="run" items="${runs}">
            <runtag:runtag value="${run}"/>
        </c:forEach>
    </div>
</div>
</body>
</html>
